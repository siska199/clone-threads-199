import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/styles/**/*.{js,ts,jsx,tsx,mdx,css,}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    container : {
      center : true,
      screens : {

      }
    },
    extend: {
      lineHeight : {
        normal : '140%'
      },
      fontSize : {
        "heading-1" : "2.25rem",
        "heading-2" : "1.875rem",
        "heading-3" : "1.5rem",
        "heading-4" : "1.25rem",
        "body"      : "1.125rem",
        "base"      : "1rem",
        "small"     : "0.875rem",
        "subtle"    : "0.75rem",
        "tiny"      : "0.625rem",
        "x-small"   : "0.438rem"
      },
      spacing: {
        xs    : "0.25rem",
        sm    : '0.5rem',
        md    : '0.75rem',
        base  : "1rem",
        lg    : '2rem',
        xl    : '3rem',
      },
      
      colors : {
        text    : '#c4bcbc',
        action :{
          purple : {
            default   : "#877EFF",
            hover     : '#ada7fa',
          },
          yellow : "#FFB620",
          red    : "#FF5A5A",
          blue   : "#0095F6",
          disabled  : {
            btn : '#ecebfa'
          },
        },
        main    : "#000000",
        primary : {
          lv1 : '#121417',
          lv2 : "#101012",
          lv3 : "#1F1F22",
          lv4 : "#3e3e42"
        },
      },
      screens: {
        'sm': '640px',  
        'md': '795px', 
        'lg': '1024px',  
        'xl': '1280px',  
        '2xl': '1536px',
      }
    },
  },
  plugins: [],
}
export default config
