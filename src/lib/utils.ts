import clsx, { ClassValue } from "clsx";
import { twMerge } from "tailwind-merge";

export const cn = (...inputs : ClassValue[])=>twMerge(clsx(inputs))

interface TParamsGetElmAttr {
    attr : 'height' | 'width';
    id : string;
}

export const handleGetElmAttr= (params:TParamsGetElmAttr)=>{
    const {attr, id} = params

    let result 
    if(attr==='height'){
        result = document.getElementById(id)?.clientHeight
    }

    if(attr==='width'){
        result = document.getElementById(id)?.clientWidth
    }
    console.log("result: ", result, params)
    return  result
}
