export const zIndex = {
    modal : 'z-[999]',
    bar   : 'z-[2]'
}

const styles =  {
    gap : {
        xs  : 'gap-xs',
        sm  : 'gap-sm',
        md  : 'gap-md',
        base: 'gap-base',
        lg  : 'gap-lg',
        xl  : 'gap-xl',       
      },
    padding : {
        xs  : 'p-xs',
        sm  : 'p-sm',
        md  : 'p-md',
        base: 'p-base',
        lg  : 'p-lg',
        xl  : 'p-xl',     
    }
}

export const styleLayoutRoot = {
    topBar      : 'h-[5rem]',
    content     : 'max-sm:ml-0 max-md:ml-[6rem] max-md:mr-0 max-lg:mr-[17rem] ml-[13rem] mr-[23rem]',
    leftSidebar : 'max-sm:hidden max-md:w-[6rem] w-[13rem]',
    rightSidebar: 'max-md:hidden max-lg:w-[17rem] w-[23rem]'
}

export default styles;

