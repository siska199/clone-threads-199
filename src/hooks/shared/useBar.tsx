import { listMenuSidebarLeft } from '@/lib/data/menu'
import { usePathname } from 'next/navigation'
import { useCallback, useEffect, useState } from 'react'

const useLeftSidebar = () => {
    const [activeMenu, setActiveMenu] = useState<string | null>()
    const pathname = usePathname()

    const handleSetActiveMenu = useCallback(()=>{
        const activePathname = listMenuSidebarLeft?.filter((menu)=>{
            const isActive =  pathname===menu.route
            return isActive
        })[0]
        setActiveMenu(activePathname.label)
    },[pathname]) 

    useEffect(()=>{
        handleSetActiveMenu()
    },[pathname, handleSetActiveMenu ])

    return{ listMenuSidebarLeft, activeMenu, }
}

export default useLeftSidebar