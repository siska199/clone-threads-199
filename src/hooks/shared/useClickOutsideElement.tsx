import { RefObject, useEffect } from "react";

type Event = MouseEvent | TouchEvent;

interface TProps <T>{
    ref     : RefObject<T>,
    handler : (event: Event) => void;
    isOpen  : boolean;
}

export const useOnClickOutside = <T extends HTMLElement = HTMLElement>(props:TProps<T>) => {
    const {ref, handler, isOpen} = props
    
    useEffect(() => {
        const listener = (event: Event) => {
        const el = ref?.current;
        if (!el || el.contains((event?.target as Node) || null)) {
            return;
        }

        isOpen && handler(event); 
    };

    document.addEventListener("mousedown", listener);
    document.addEventListener("touchstart", listener);

    return () => {
      document.removeEventListener("mousedown", listener);
      document.removeEventListener("touchstart", listener);
    };
  }, [ref, handler]); 
};