import React from 'react';
import { FcGoogle } from "react-icons/fc";
import Button from '../shared/Button';

interface TProps {
  // Add your prop types here
}

const SignInWithSocialMedia: React.FC<TProps> = async(props) => {
  return (
    <div className=' w-full'>
      <Button
        variant={'purple-outlined'}
        className='w-full'
        Icon={FcGoogle}
        labelBtn={{
          text : 'Sign in with google',
        }}
      />
    </div>
  );
};

export default SignInWithSocialMedia;