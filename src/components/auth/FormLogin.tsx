import React from 'react';
import ContainerFlex from '../shared/container/ContainerFlex';
import InputBase from '../shared/input/InputBase';
import { cn } from '@/lib/utils';
import Typography from '../shared/Typography';
import Button from '../shared/Button';
import LinkCustome from '../shared/LinkCustome';
import SignInWithSocialMedia from './SignInWithSocialMedia';

interface TProps {
  // Add your prop types here
}

const FormLogin: React.FC<TProps> = (props) => {
  return (
    <Custome.ContainerForm customElement='form' className={cn('bg-primary-lv1 w-[20rem]')} gap={'md'} padding={"lg"} variant={"vsc"}>
      <Typography variant={"heading-3"} className='text-center' weight={"bold"}>Login</Typography>

      <>
        <InputBase label={"Username"} placeholder='siska199'/>
        <InputBase label={"Password"} type="password" placeholder='*****'/>
      </>

      <Button 
        className='w-full mt-base'
        labelBtn={{
          text: 'Login',
        }}
      />

      <Typography className='text-center'  withMiddleLine={true}>
        OR
      </Typography>

      <SignInWithSocialMedia/>

      <LinkCustome
        variant={"variant2"}
        route={"/sign-up"}
        labelLink = {{
          text      : "Don't have an account ?",
          className : 'text-center text-small'
        }}
        className='w-full'
      />

    </Custome.ContainerForm>
  );
};


const Custome = {
  ContainerForm : ContainerFlex
}

export default FormLogin;