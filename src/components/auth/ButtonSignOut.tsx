import React from 'react';
import Button from '../shared/Button';
import clsx from 'clsx';

interface TProps {
  consumer : 'leftSidebar' | 'topBar'
}

const ButtonSignOut: React.FC<TProps> = (props) => {
    const {consumer} = props
  return (
      <Button
        className={clsx({
            'md:!w-full '  : consumer==='leftSidebar',
            'md:hidden' : consumer==='topBar',
        })}
        labelBtn={{text:'SignOut', className:clsx({
            'max-md:hidden max-md:p-sm' : consumer==='leftSidebar',
            'hidden' : consumer==='topBar'
        })}} 
        position='start'
        variant={"transparent"} 
        image={{url: "/assets/logout.svg", alt:"Logout icon",width:24,height:24,}}
    />
  );
};

export default ButtonSignOut;