"use client"
import ButtonSignOut from '@/components/auth/ButtonSignOut'
import useLeftSidebar from '@/hooks/shared/useBar'
import { cn } from '@/lib/utils'
import { styleLayoutRoot } from '@/styles/styles-config'
import { FC } from 'react'
import LinkCustome from '../LinkCustome'
import ContainerFlex from '../container/ContainerFlex'

const LeftSidebar : FC = () => {
  const { listMenuSidebarLeft,activeMenu, } = useLeftSidebar()
  
  return (
    <section
      className={cn('bg-primary-lv1 h-screen fixed left-0 bottom-0', styleLayoutRoot.leftSidebar)}
      style={{
        top : "5rem"
      }}
    >
      <Custome.ContainerContent 
        variant={"vcs"} 
        customElement='ul' 
        padding={'base'} 
        gap="md" 
        className='h-[calc(100%-5rem)] flex-nowrap overflow-y-auto'
      >
          {
            listMenuSidebarLeft?.map((menu)=>(
              <li key={menu.label} className='flex justify-center md:justify-start w-full'>
                <LinkCustome
                  variant={"variant1"}
                  route={menu.route}
                  labelLink={{text:menu.label, className:'max-md:hidden '}}
                  image={{url:menu.imgURL, alt:menu.label, width:24,height:24}}
                  isActive={menu.label===activeMenu}
                  className='p-sm md:w-full '
                />
              </li>
            ))
          }

          <li key={"Sign Out"} className='w-full mt-auto px-sm '>
            <ButtonSignOut consumer='leftSidebar'/>
          </li>
      </Custome.ContainerContent>
    </section>
  )
}

const Custome = {
  ContainerContent : ContainerFlex,
}


export default LeftSidebar