import { cn } from '@/lib/utils';
import { styleLayoutRoot } from '@/styles/styles-config';
import Dropdown from '../Dropdown';
import LinkCustome from '../LinkCustome';
import ContainerFlex from '../container/ContainerFlex';
import Avatar from '../Avatar';
import Typography from '../Typography';
import ButtonSignOut from '@/components/auth/ButtonSignOut';

const Topbar = () => {

  return (
    <Custome.Nav variant={"hbc"} customElement='nav' className={cn('container sticky z-[3] top-0 p-sm px-base bg-primary-lv1',styleLayoutRoot.topBar)}>
      <LinkCustome
        variant={"variant2"}
        route={"/"}
        labelLink={{text:"Thread",variant:"heading-3", weight:"bold", className:'items-center gap-4 flex'}}
        image={{url:"/logo.svg", alt:"logo", width:28,height:28}}
        className='!p-sm'
      />
      <ContainerFlex variant={'hsc'} gap={"sm"} className='w-fit'>
        <ButtonSignOut consumer='topBar' />
        <Dropdown 
          listMenuDropdown={[]}
          childrenBtn={
            <>
              <Avatar url={'/assets/user.svg'} alt={'Icon profile user'} sizeImg={"sm"}/>
              <Typography weight={"bold"}>Siska199</Typography>
            </>
          }  
        />
      </ContainerFlex>
    </Custome.Nav>
  )
}

const Custome = {
  Nav : ContainerFlex
}

export default Topbar