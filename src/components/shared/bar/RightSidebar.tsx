"use client"
import { FC } from 'react'
import ContainerFlex from '../container/ContainerFlex'
import { cn } from '@/lib/utils'
import { styleLayoutRoot } from '@/styles/styles-config'

const RightSidebar : FC = () => {
 
  return (
      <Custome.ContainerContent 
        variant={"vcs"} 
        customElement='ul' 
        padding={'base'} 
        gap="sm" 
        className={cn('bg-primary-lv1 h-screen  fixed  right-0 bottom-0 ', styleLayoutRoot.rightSidebar)}
        style={{
          top : "5rem"
        }}
      >
        section    
      </Custome.ContainerContent>

  )
}

const Custome = {
  ContainerContent : ContainerFlex,
}


export default RightSidebar