import React, { HTMLProps } from 'react';
import ContainerFlex from '../container/ContainerFlex';
import { cn } from '@/lib/utils';
import Typography from '../Typography';

interface TProps  extends HTMLProps<HTMLInputElement>{
    label? : string;
    direction? : 'vertical' | 'horizontal',
    error? : string;
}

const InputBase: React.FC<TProps> = (props) => {
    const {label,direction="vertical",error, ...attrs} = props

    return (
        <ContainerFlex variant={direction==='vertical'? "vsc" : "hsc"} gap="xs" className={cn('w-full !p-0')}>
            {label && <label className='text-small'>{label}</label>}
            <input 
                className={cn('text-small w-full bg-primary-lv2 border-primary-lv3 border-2 outline-none py-2 cursor-text px-2 rounded-md')}
                {...attrs}
            />
            {error && <Typography classID='text-sm'>{error}</Typography>}            
        </ContainerFlex>
    );
};

export default InputBase;