import { cn } from '@/lib/utils';
import { VariantProps, cva } from 'class-variance-authority';
import Image from 'next/image';
import React, { HTMLProps } from 'react';
import Typography, { TTypeVariantTypography, TTypeWeightTypography } from './Typography';

const buttonVariants = cva(
  'w-fit p-sm rounded-lg items-center disable:bg-action-disabled-btn flex gap-sm  animation-click-btn',
  {
    variants : {
      variant : {
        'purple-contained'  : 'text-white bg-action-purple-default hover:bg-action-purple-hover',
        'purple-outlined'   : 'border-2 border-action-purple-default hover:bg-white/10 hover:border-action-purple-hover',
        'transparent'       : 'bg-transparent hover:bg-white/10'
      },
      isReverse : {
        "true"  : "flex-row-reverse border	",
        "false" : ""
      },
      position : {
        'center'  : 'justify-center',
        'start'   : 'justify-start',
        'end'     : 'justify-end'
      }
    },
    defaultVariants : {
      variant : 'purple-contained',
      position : 'center'
    }
  }
)

interface TProps extends HTMLProps<HTMLButtonElement>, VariantProps<typeof buttonVariants > {
  labelBtn? : {
      text : string;
      variant? : TTypeVariantTypography;
      weight?  : TTypeWeightTypography;
      className? : string;
  },
  Icon? : React.ElementType;
  image? : {
      url   : string;
      alt    : string;
      width  : number;
      height : number;
      position? : 'left' | 'right';
  },
  loading? : boolean;
  position? : 'center' | 'start' | 'end'
}

const Button = (props: TProps) => {
  const {children, className,labelBtn, image,Icon,position,variant,loading=false, ...attrs} = props
  const Component = 'button' as React.ElementType

  const childBtn = children ?? 
                    <>
                      {
                        loading  ? 
                        <Typography>Loading...</Typography>
                        :
                        <>
                          {Icon && <Icon/>}
                          {image && <Image src={image.url} alt={image.alt} width={image.width} height={image.height}/>}
                          {labelBtn &&  <Typography variant={labelBtn.variant} weight={labelBtn.weight} className={cn('!w-fit',labelBtn.className)}>{labelBtn.text}</Typography>}
                        </>
                      }
                    </>
  return (
    <Component 
        className={cn(buttonVariants({
          isReverse:image?.position ==="right",
          variant,
          position,
          className,
      }))}
      {...attrs}
    >
      {childBtn}
    </Component>

  )
}

export default Button