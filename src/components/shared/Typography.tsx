import React from 'react'
import {VariantProps, cva} from 'class-variance-authority'
import { cn } from '@/lib/utils'

export type TTypeVariantTypography    = "heading-1" | "heading-2" | "heading-3" |"heading-4" | "body" |"base"|'small'|'subtle'|'tiny'|'x-small'
export type TTypeWeightTypography     = "bold" | "semi-bold" | "medium" | "normal"

const typographyVariants = cva(
    'leading-[140%] w-full text-start ',
    {
        variants : {
            variant : {
                "heading-1" : 'text-heading-1',
                "heading-2" : "text-heading-2",
                "heading-3" : "text-heading-3",
                "heading-4" : "text-heading-4",
                "body"      : "text-body",
                "base"      : "text-base",
                "small"     : "text-small",
                "subtle"    : "text-subtle !leading-[1rem]",
                "tiny"      : "text-tiny",
                "x-small"   : "text-x-small !leading-[0.582rem]"
            },
            weight : {
                "bold"      : "!font-[700]",
                "semi-bold" : "!font-[600]",
                "medium"    : "!font-[500]",
                "normal"    : "!font-[400]",
                "thin"      : "!font-[300]" 
            },
            withMiddleLine : {
                "true" : " grid-cols-3 grid items-center gap-x-[0.1rem] before:content[''] before:h-[2px] before:bg-primary-lv3 after:block after:content[''] after:h-[2px] after:bg-primary-lv3 after:block",
                "false" : ""
            }
        },
        defaultVariants : {
            variant : 'base',
            weight : 'normal'
        }

    }
)

interface TProps extends React.HTMLProps<HTMLParagraphElement | HTMLHeadingElement>, VariantProps<typeof typographyVariants> {
    children        : React.ReactNode;
}

const Typography = (props: TProps) => {
    const {variant, weight, className, children,withMiddleLine,...attrs} = props
    const Component  = generateComponentTag(variant!) as React.ElementType

    return (
    <Component className={cn(typographyVariants({variant, weight, withMiddleLine, className}))}  {...attrs}>
        {children}
    </Component>
  )
}


const generateComponentTag = (variant: TTypeVariantTypography) : React.ElementType=>{

    let Component = "p"
    switch(variant){
        case 'heading-1':
            Component = "h1"
            break;
        case 'heading-2':
            Component = "h2"
            break;
        case 'heading-3':
            Component = "h3"
            break;
        case 'heading-4':
            Component = "h4"
            break;
        case 'body':
        case 'base':
        case 'subtle':
        case 'tiny':
        case 'x-small':
            Component = "p"
            break;
        
    }

    return Component as React.ElementType
}

export default Typography