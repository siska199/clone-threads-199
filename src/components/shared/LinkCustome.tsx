import Link from 'next/link'
import React, { HTMLProps } from 'react'
import Typography, { TTypeVariantTypography, TTypeWeightTypography } from './Typography'
import Image from 'next/image';
import { VariantProps, cva } from 'class-variance-authority';
import { cn } from '@/lib/utils';

const linkCustomeVariants = cva(
    'flex p-base gap-sm rounded-lg t w-fit h-full animation-click-btn ',
    {
        variants : {
            variant : {
                variant1 : "hover:bg-action-purple-default",
                variant2 : "hover:underline underline-offset-8 !p-0 "
            },
            isActive : {
                "true" : "bg-action-purple-default",
                "false" : ""
            },
            isReverse : {
                "true"  : "flex-row-reverse border	",
                "false" : ""
            }
        },
        defaultVariants : {
            variant : "variant1"
        }
    },
)

interface TProps extends HTMLProps<HTMLElement>, VariantProps<typeof linkCustomeVariants>{
    route : string;
    labelLink : {
        text : string;
        variant? : TTypeVariantTypography;
        weight?  : TTypeWeightTypography;
        className? : string;
    },
    image? : {
        url     : string;
        alt     : string;
        width  : number;
        height : number;
        position? : 'left' | 'right'
    },

}

const LinkCustome = (props: TProps) => {
    const {route, labelLink, image, variant, isActive,className} = props
  return (
    <Link href={route} className={cn(linkCustomeVariants({
        className,
        isReverse:image?.position ==="right",
        variant,
        isActive,
    }))}>
        {
            image && <Image src={image.url} alt={image.alt} width={image.width} height={image.height}/>
        }
        <Typography variant={labelLink.variant} weight={labelLink.weight} className={labelLink.className}>{labelLink.text}</Typography>
    </Link>
  )
}

export default LinkCustome