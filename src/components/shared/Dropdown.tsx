"use client"

import clsx from 'clsx';
import React, { useRef, useState } from 'react';
import Button from './Button';
import { useOnClickOutside } from '@/hooks/shared/useClickOutsideElement';

interface TProps {
  childrenBtn : React.ReactNode;
  listMenuDropdown : {
    id      : string;
    label   : string;
    onClick : ()=>void;
  }[];

}

const Dropdown: React.FC<TProps> = (props) => {
  const {listMenuDropdown, childrenBtn} = props
  const dropdownElemn = useRef<null | HTMLDivElement>(null)
  const [showDropdown, setShowDropdown] = useState(false)
  
  const handleToggleDropdown = ()=>{
    setShowDropdown(!showDropdown)
  }
  
  useOnClickOutside({ref:dropdownElemn,isOpen :showDropdown , handler:handleToggleDropdown})  
showDropdown
  return (
    <div ref={dropdownElemn} className='relative border-primary-lv4 border rounded-md'>
      <Button onClick={handleToggleDropdown} variant={'transparent'}>
        {childrenBtn}
      </Button>
      {
        showDropdown && 
        <ul className='bg-primary-lv3 py-sm rounded-md absolute top-[2.7rem] left-0 w-full'>
          {
            listMenuDropdown.map((menu,i)=>(
              <li key={menu.id} className={clsx('px-base py-xs hover:bg-primary-lv4 cursor-pointer ')}>
                {menu.label}
              </li>
            ))
          }
        </ul>
      }
    </div>

  );
};

export default Dropdown;