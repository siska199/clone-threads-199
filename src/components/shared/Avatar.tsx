import { cn } from '@/lib/utils';
import { VariantProps, cva } from 'class-variance-authority'
import Image from 'next/image';
import React, { HTMLProps } from 'react'

const avatarVariants = cva(
  '',
  {
    variants : {
      variant : {
        circle : "rounded-full",
        square : "rounded-md",
      },
      withAnimation : {
        "true" : "animation-click-btn",
        "false" : ""
      }
    },
    defaultVariants : {
      variant       : "circle",
      withAnimation : false
    }
  }
)

interface TProps extends HTMLProps<HTMLImageElement>, VariantProps<typeof avatarVariants > {
  url   : string;
  alt   : string;
  sizeImg  : "sm" | "md" | "lg"
}

const Avatar = (props: TProps) => {
  const {sizeImg, url, alt, variant, withAnimation} = props
  const {height, width} = generateWidhtHeightImg(sizeImg)
  return (
    <Image
      src={url || '/assets/user.svg'}
      alt={alt}
      width={width}
      height={height}
      className={cn(avatarVariants({
        variant,
        withAnimation
      }))}
    />
  )
}

const generateWidhtHeightImg = (sizeImg : TProps["sizeImg"])=>{
  let height = 0
  let width = 0

  switch(sizeImg){
    case "sm":
      height = 24
      width = 24
      break;
    case "md" :
      height = 32
      width = 32
      break;
    case "lg":
      height = 42
      width = 42
      break;
  }
  return {height, width}
}

export default Avatar