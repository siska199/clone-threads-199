import ContainerFlex from './ContainerFlex'

interface TProps {
}

const ContainerModal = (props: TProps) => {

  return (
    <Comp.ContainerModal variant={"vcc"} className={'container-modal'}>
      <Comp.ContainerContent >
        <section className="header">

        </section>
        <section className="body">

        </section>
        <section className="section">

        </section>
      </Comp.ContainerContent>
    </Comp.ContainerModal>
  )
}

const Comp = {
  ContainerModal : ContainerFlex,
  ContainerContent : ContainerFlex
}

export default ContainerModal