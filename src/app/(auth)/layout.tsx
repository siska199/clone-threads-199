import ContainerFlex from '@/components/shared/container/ContainerFlex';
import React from 'react';

interface TProps {
  children : React.ReactNode;
}

const AuthLayout: React.FC<TProps> = (props) => {
    const {children} = props

    return (
        <ContainerFlex variant={"vcc"} customElement='main' className='container-layout'>
            {children}
        </ContainerFlex>
    );
};

const Custome = {
    Main   : ContainerFlex,
}

export default AuthLayout;