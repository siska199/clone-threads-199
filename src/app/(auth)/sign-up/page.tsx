import FormRegister from '@/components/auth/FormRegister';
import React from 'react';

interface TProps {
  // Add your prop types here
}

const SignUpPage: React.FC<TProps> = (props) => {
  return (
    <article>
        <FormRegister/>
    </article>
  );
};

export default SignUpPage;