import FormLogin from '@/components/auth/FormLogin';
import React from 'react';

interface TProps {
  // Add your prop types here
}

const SignInPage: React.FC<TProps> = (props) => {
  return (
    <article>
      <FormLogin/>
    </article>
  );
};

export default SignInPage;