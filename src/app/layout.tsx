import ContainerFlex from '@/components/shared/container/ContainerFlex'
import { cn } from '@/lib/utils'
import '../styles/global.css'
import { Inter } from 'next/font/google'
import React, { FC } from 'react'

const inter = Inter({ subsets: ['latin'] })

interface TProps {
  children : React.ReactNode;
}

const RootLayout : FC<TProps> = (props) => {
  const {children} = props

  return (
    <html lang="en">
      <Custome.Body className={cn(inter.className, '!p-0 !gap-0 m-auto')} customElement='body' variant={"vss"} >        
        {children}
      </Custome.Body>
    </html>
  )
}



const Custome = {
  Body    : ContainerFlex,
}

export default RootLayout

