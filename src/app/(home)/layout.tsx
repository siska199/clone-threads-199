import LeftSidebar from '@/components/shared/bar/LeftSidebar'
import RightSidebar from '@/components/shared/bar/RightSidebar'
import Topbar from '@/components/shared/bar/Topbar'
import { cn } from '@/lib/utils'
import { styleLayoutRoot } from '@/styles/styles-config'
import '@/styles/global.css'
import React, { FC } from 'react'

interface TProps {
  children : React.ReactNode;
}

const RootLayout : FC<TProps> = (props) => {
  const {children} = props

  return (
    <>
      <Topbar/>
      <main className='container-layout flex relative'>
        <LeftSidebar/>
        <section className={cn('flex-grow p-base',styleLayoutRoot.content)}>
          {children}
        </section>
        <RightSidebar/>
      </main>
    </>
  )
}





export default RootLayout

